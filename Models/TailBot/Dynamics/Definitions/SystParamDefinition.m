% *************************************************************************
%
% Msc THESIS STEVE HEIM 2014
%
% function [systParamVec, systParamNames, systParamIndices] = SystParamDefinition()
% function p = SystParamDefinition()
%
% This MATLAB function defines the physical system parameter vector 'p' for
% a prismatic monopod with tail in 2D.  Besides serving as initial configuration of
% the model, this file provides a definition of the individual components
% of the system parameter vector and an index struct that allows name-based
% access to its values.
%
% NOTE: This function is relatively slow and should not be executed within
%       the simulation loop.
%
% Input:  - NONE
% Output: - The initial system parameters as the vector 'systParamVec' (or 'p')
%         - The corresponding parameter names in the cell array 'systParamNames' 
%         - The struct 'systParamIndices' that maps these names into indices  
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%     buffintk@bucknell.edu
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%
%   See also HYBRIDDYNAMICS, FLOWMAP, JUMPMAP, JUMPSET, 
%            CONTSTATEDEFINITION, DISCSTATEDEFINITION, EXCTSTATEDEFINITION,
%            EXCTPARAMDEFINITION, 
%            VEC2STRUCT, STRUCT2VEC. 
%
function [systParamVec, systParamNames, systParamIndices] = SystParamDefinition()
    
% REMY keeps a mass for the 'thigh' (upper leg), equivalent to my hip.
% Probably for numerical reasons (computing resulting forces)?

    % All units are normalized to gravity g, total mass m_0, and
    % uncompressed leg length kF0.
    
    % Physics:
    systParam.g         = 9.81;     % [g] gravity
    % Parameter of the model
%     systParam.a_0   = 0;     % [rad] resting leg angle
    systParam.mS        = 1;  % [m_0] mass of the main body
    systParam.mT        = 0.15;  % [m_0] mass of the tail     
    systParam.mF        = 0.05;  % [m_0] mass of the foot
    systParam.lxC       = 0;  % [lxC] distance in x between CoG of main body and hip joint
    systParam.lyC       = 0;  % [lyC] distance in y between CoG of main body and hip joint
%     systParam.rFoot     = 0.05;  % [l_0] foot radius % removed
    systParam.lxB       = 0;  % [lxC] distance in x between CoG of main body and tail joint
    systParam.lyB       = 0;  % [lyC] distance in y between CoG of main body and tail joint
    systParam.lT        = 0.3;   % [lT] length of tail
    systParam.jS        = 1;   % [m_0*l_0^2] inertia of the main body
    systParam.jT        = 0.03; % [m_0*l_0^2] inertia of the tail
    systParam.jF        = 0.04; % [m_0*l_0^2] inertia of the foot
%     systParam.kalpha    = 5;     % [m_0*g*l_0/rad] rotational spring stiffness in the hip joint  
%     systParam.balphaRat = 0.2;   % [*] damping ratio.  Use 20% of critical damping
    systParam.kF        = 250;    % [m_0*g/l_0] linear spring stiffness in the prismatic joint 
    systParam.kF0       = 0.65;     % [kF0] uncompressed leg length
    systParam.bF        = 8;   % [*] damping ratio. 
    
    systParam.kH        = 5;
    systParam.kH0       = -pi/4;
    systParam.bH        = 2;
    
    systParam.kT        = 5;
    systParam.kT0       = 0;
    systParam.bT        = 2;
    
    systParam.motorResF = 0.01;  % motor resistance / (motor constant)^1/2
    systParam.motorResH = 0.01;  % So that electrical power loss can be calculated as:
    systParam.motorResT = 0.01;  % motorTorque^2*motorRes, (where motorTorque is the control output, and motor current is motorTorque/motorConstant
    systParam.touchDownTerminal = false; % An attribute of simulation: turn it on to make landing a terminal condition (see JumpMap);
    systParam.liftOffTerminal = false; % An attribute of simulation: turn it on to make take-off a terminal condition (see JumpMap);
    systParam.apexTerminal = false;
    %     systParam.nDoF      = 6;     % number of degrees of freedom. This is half the number of states
   
    [systParamVec, systParamNames] = Struct2Vec(systParam);
    systParamIndices = Vec2Struct(1:1:length(systParamVec),systParamNames);
end
