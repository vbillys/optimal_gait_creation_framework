function footPts = FootPtPositions(x,y,phi,yF,phiH,phiT,g,lxC,lyC,lxB,lyB,lT,mS,mF,mT,jS,jF,jT,kF,kF0,bF)
%FOOTPTPOSITIONS
%    FOOTPTS = FOOTPTPOSITIONS(X,Y,PHI,YF,PHIH,PHIT,G,LXC,LYC,LXB,LYB,LT,MS,MF,MT,JS,JF,JT,KF,KF0,BF)

%    This function was generated by the Symbolic Math Toolbox version 5.4.
%    08-Jul-2014 11:39:48

t221 = phi+phiH;
t222 = kF0+yF;
t223 = cos(phi);
t224 = sin(phi);
footPts = [x+lxC.*t223+lyC.*t224-t222.*sin(t221);y-lxC.*t224+lyC.*t223-t222.*cos(t221)];
