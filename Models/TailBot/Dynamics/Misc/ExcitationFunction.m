% *************************************************************************
%
%
% Msc THESIS STEVE HEIM 2014
%
%  function u = ExcitationFunction(y, z, s)
%
% This MATLAB file defines the excitation function h used in a prismatic
% monopod with tail in 2D.  Its output u is depending on the continuous states 'y'
% and  discrete states 'z', as well as the excitation parameter vector 's'.
%
%
% Input:  - A vector of continuous states 'y'
%         - A vector of discrete states 'z'
%         - A vector of excitation parameters 's';
%
% Output: - the states of the series elastic driver: 'u'
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems,
%     Swiss Federal Institute of Technology (ETHZ)
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering,
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also HYBRIDDYNAMICS, FLOWMAP, JUMPSET, JUMPSET,
%            CONTSTATEDEFINITION, DISCSTATEDEFINITION, EXCTSTATEDEFINITION,
%            EXCTPARAMDEFINITION.
%
function u = ExcitationFunction(y, ~, s)

% Get a mapping for the state and parameter vectors.
% Keep the index-structs in memory to speed up processing

persistent exctStateIndices exctParamIndices contStateIndices discStateIndices;

if isempty(exctStateIndices) || isempty(exctParamIndices) || isempty(contStateIndices)
    [~, ~, exctStateIndices] = ExctStateDefinition();
    [~, ~, exctParamIndices] = ExctParamDefinition();
    [~, ~, contStateIndices] = ContStateDefinition();
    [~,~,discStateIndices]   = DiscStateDefinition();
    %         counter = 1;
end
u = zeros(3,1); % makes this general
    
    xi = y(contStateIndices.time)*2*pi*s(exctParamIndices.strideFreq);

    u = zeros(3,1); % Torques and forces

    % Create fourier series by addition over all elements:
    for i = 1:length(exctParamIndices.sinphiH)
        u(exctStateIndices.phiH)   = u(exctStateIndices.phiH) + s(exctParamIndices.sinphiH(i))*sin(xi*i) + s(exctParamIndices.cosphiH(i))*cos(xi*i);
%         u(exctStateIndices.dul)  = u(exctStateIndices.dul)  + (+s(exctParamIndices.sinl(i))*cos(phi*i) - s(exctParamIndices.cosl(i))*sin(phi*i))*(dphi*i);
    end
u(exctStateIndices.phiH)   = u(exctStateIndices.phiH) + s(exctParamIndices.offsetphiH);
    for i = 1:length(exctParamIndices.sinyF)
        u(exctStateIndices.yF)   = u(exctStateIndices.yF) + s(exctParamIndices.sinyF(i))*sin(xi*i) + s(exctParamIndices.cosyF(i))*cos(xi*i);
%         u(exctStateIndices.dyF)  = u(exctStateIndices.dyF)  + (+s(exctParamIndices.sinyF(i))*cos(phi*i) - s(exctParamIndices.cosyF(i))*sin(phi*i))*(dphi*i);
    end
u(exctStateIndices.yF)   = u(exctStateIndices.yF) + s(exctParamIndices.offsetyF);
    for i = 1:length(exctParamIndices.sinphiT)
        u(exctStateIndices.phiT)   =u(exctStateIndices.phiT) + s(exctParamIndices.sinphiT(i))*sin(xi*i) + s(exctParamIndices.cosphiT(i))*cos(xi*i);
%         u(exctStateIndices.dul)  = u(exctStateIndices.dul)  + (+s(exctParamIndices.sinl(i))*cos(phi*i) - s(exctParamIndices.cosl(i))*sin(phi*i))*(dphi*i);
    end
u(exctStateIndices.phiT)   = u(exctStateIndices.phiT) + s(exctParamIndices.offsetphiT);
    

end
