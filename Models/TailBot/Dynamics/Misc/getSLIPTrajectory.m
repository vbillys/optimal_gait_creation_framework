function [yOUT] = getSLIPTrajectory(yVec,p,tspan)
% Adjust to use tStep to create tspan dynamically!
% x,y,phi,yF,phiH, dx,dy,dphi,dyF,dphiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF

% Basically the simulation and Flow Map from a SLIP model derived from
% Remy's simulation framework

% unwrap


persistent contStateIndices
if isempty(contStateIndices)
    [~, ~, contStateIndices] = ContStateDefinition();
end

% yIN = [yVec(contStateIndices.x);
%     yVec(contStateIndices.y);
%     yVec(contStateIndices.phi);
%     yVec(contStateIndices.yF);
%     yVec(contStateIndices.phiH);
%     yVec(contStateIndices.dx);
%     yVec(contStateIndices.dy);
%     yVec(contStateIndices.dphi);
%     yVec(contStateIndices.dyF);
%     yVec(contStateIndices.dphiH)];

%     p = p_;
persistent systParamIndices
if isempty(systParamIndices)
    [~, ~, systParamIndices] = SystParamDefinition();
end
g   = p(systParamIndices.g);
lxC = p(systParamIndices.lxC);
lyC = p(systParamIndices.lyC);
%     lxB = p(systParamIndices.lxB);
%     lyB = p(systParamIndices.lyB);
%     lT = p(systParamIndices.lT);
mS = p(systParamIndices.mS);
mF = p(systParamIndices.mF);
mT = p(systParamIndices.mT);
jS = p(systParamIndices.jS);
jF = p(systParamIndices.jF);
%     jT = p(systParamIndices.jT);
kF = p(systParamIndices.kF);
kF0 = p(systParamIndices.kF0);
bF = p(systParamIndices.bF);
mS = mS+mT;%+mF; % add tail weight to body weight. Also add Foot: this way impact loss of foot is compensated, system remains conservative
%%

odeOPTIONS = odeset('RelTol',1e-6,'AbsTol',1e-12,'MaxStep',0.01);
odeOPTIONS = odeset(odeOPTIONS,'Events',@Events);
yVec
[tOUT,yOUT,teOUT,yeOUT,ieOUT] = ode45(@ODE,tspan,yVec,odeOPTIONS);
N = numel(tOUT);
yOUT = reshape(yOUT,N,14); %reshape each state into it's own column
% It's in [x dx y dy ...]!!! NECESSARY for using contSateIndices

% yOUT(:,13:14) = []; % Deleting time vector and CoT entries UNNECESSARY,
% have "getSystemStates" to handle this
% yOUT = cat(2,yOUT(:,1:2:end),yOUT(:,2:2:end));

%%

    function [value_,isTerminal_,direction_] = Events(~,y_)
        % basically just lift-off condition for SLIP during stance
        % unwrap
        x = y_(contStateIndices.x);
        y = y_(contStateIndices.y);
        phi = y_(contStateIndices.phi);
        yF = y_(contStateIndices.yF);
        phiH = y_(contStateIndices.phiH);
        dx     = y_(contStateIndices.dx);
        dy     = y_(contStateIndices.dy);
        dphi   = y_(contStateIndices.dphi);
        dyF    = y_(contStateIndices.yF);
        dphiH  = y_(contStateIndices.dphiH);
        % Compute the differentiable force vector (i.e. coriolis forces,
        % gravity, and actuator forces):
        f_diff = F_CoriGravSLIP(x,y,phi,yF,phiH, dx,dy,dphi,dyF,dphiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF);
        % Mass matrix
        M = MassMatrixSLIP(x,y,phi,yF,phiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF);
        % Contact Jacobian:
        J    = ContactJacobianSLIP(x,y,phi,yF,phiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF);
        dJdtTIMESdqdt = ContactJacobianDtTIMESdqdtSLIP(x,y,phi,yF,phiH,dx,dy,dphi,dyF,dphiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF);
        % Requirement for a closed contact is that the contact point
        % acceleration is zero:
        % J*dqddt + dJdt*dqdt = 0
        % with EoM:
        % dqddt = M_inv*(f_diff + J'*f_cont)
        % -> J*M_inv*(f_diff + J'*f_cont) + dJdt*dqdt = 0
        % -> J*M_inv*f_diff + J*M_inv*J'*f_cont + dJdt*dqdt = 0
        % -> f_cont = inv(J*M_inv*J')*(-J*M_inv*f_diff - dJdt*dqdt)
        f_contX = (J*(M\J'))\(-J*(M\f_diff) - dJdtTIMESdqdt);
        % Event is detected when vertical contact forces becomes negative:
        
        value_ = - f_contX(2) - 0.005; % Add a little offset to eliminate numerical jitter
        % This offset seemS rather large....
        isTerminal_ = true;
        direction_  = 1;
        
        % underscores for consistency with Remy's simulation. Beats the
        % hell out of me why otherwise....
        
    end

    function dydt = ODE(~,y_)
        dydt = zeros(size(y_));
        
        % unwrap
        x = y_(contStateIndices.x);
        y = y_(contStateIndices.y);
        phi = y_(contStateIndices.phi);
        yF = y_(contStateIndices.yF);
        phiH = y_(contStateIndices.phiH);
        dx     = y_(contStateIndices.dx);
        dy     = y_(contStateIndices.dy);
        dphi   = y_(contStateIndices.dphi);
        dyF    = y_(contStateIndices.yF);
        dphiH  = y_(contStateIndices.dphiH);
        
        % Map velocities to position derivatives
        dydt(contStateIndices.x)      = y_(contStateIndices.dx);
        dydt(contStateIndices.y)      = y_(contStateIndices.dy);
        dydt(contStateIndices.phi)    = y_(contStateIndices.dphi);
        dydt(contStateIndices.yF)     = y_(contStateIndices.dyF);
        dydt(contStateIndices.phiH)   = y_(contStateIndices.dphiH);
%         dydt(contStateIndices.phiT)   = y_(contStateIndices.dphiT);
        
        dydt(contStateIndices.time)   = 1;
        
        M = MassMatrixSLIP(x,y,phi,yF,phiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF);
        
        f_diff = F_CoriGravSLIP(x,y,phi,yF,phiH, dx,dy,dphi,dyF,dphiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF); % SLIP has no actuation, so f_diff = f_act + f_cori = f_cori
        f_springDampers = [0,0,0,-kF*yF,0]';  % this is termed 'actuation' in Remy's
        f_diff = f_diff+f_springDampers;
            %+ bF*( -y(contStateIndices.dyF))
        %         [~, J, dJdtTIMESdqdt] = ContactKinematicsSLIP(y_, p); % Always in Stance
        J    = ContactJacobianSLIP(x,y,phi,yF,phiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF);
        dJdtTIMESdqdt = ContactJacobianDtTIMESdqdtSLIP(x,y,phi,yF,phiH,dx,dy,dphi,dyF,dphiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF);
        % Requirement for contact (contact constraint):
        % Contact point acceleration must be zero:
        % J*dqddt + dJdt*dqdt = 0
        % with EoM:
        % dqddt = inv(M)*(f_diff + J'*f_cont)
        % -> J*inv(M)*(f_diff + J'*f_cont) + dJdt*dqdt = 0
        % -> J*inv(M)*f_diff + J*inv(M)*J'*f_cont + dJdt*dqdt = 0
        % -> f_cont = inv(J*inv(M)*J')*(-J*inv(M)*f_diff - dJdt*dqdt)
        f_contX = (J*(M\J'))\(-J*(M\f_diff) - dJdtTIMESdqdt);
        % Project these forces back into the generalized coordinate space
        f_contQ = J'*f_contX;
        dd_q = M\(f_diff + f_contQ);
        
        % Map the generalized accelerations back into continuous state
        % derivatives:
        dydt(contStateIndices.dx)      = dd_q(1);
        dydt(contStateIndices.dy)      = dd_q(2);
        dydt(contStateIndices.dphi)    = dd_q(3);
        dydt(contStateIndices.dyF)     = dd_q(4);
        dydt(contStateIndices.dphiH)   = dd_q(5);
%         dydt(contStateIndices.dphiT)   = 0; 
    end

end



function M = MassMatrixSLIP(x,y,phi,yF,phiH,g,lxC,lyC,mS,mF,jS,jF,kF,kF0,bF)
%MASSMATRIX
%    M = MASSMATRIX(X,Y,PHI,YF,PHIH,G,LXC,LYC,mS,MF,JS,JF,KF,KF0,BF)

%    This function was generated by the Symbolic Math Toolbox version 5.11.
%    11-May-2014 14:15:07
t2 = phi-phiH;
t3 = cos(t2);
t4 = kF0+yF;
t5 = mF.*t3.*t4;
t6 = mF+mS;
t7 = sin(t2);
t8 = mF.*t4.*t7;
t9 = kF0.^2;
t10 = mF.*t9;
t11 = yF.^2;
t12 = mF.*t11;
t13 = kF0.*mF.*yF.*2.0;
t14 = mF.*t7;
t15 = jF-t10-t12-t13;
M = reshape([t6,0.0,t5,t14,-t5,0.0,t6,t8,-mF.*t3,-t8,t5,t8,jF+jS+t10+t12+t13,0.0,t15,t14,-mF.*t3,0.0,mF,0.0,-t5,-t8,t15,0.0,jF+t10+t12+t13],[5, 5]);
end
%
% function footPts = FootPtPositions(x,y,phi,yF,phiH,g,lxC,lyC,mS,mF,jS,jF,kF,kF0,bF)
% %FOOTPTPOSITIONS
% %    FOOTPTS = FOOTPTPOSITIONS(X,Y,PHI,YF,PHIH,G,LXC,LYC,mS,MF,JS,JF,KF,KF0,BF)
%
% %    This function was generated by the Symbolic Math Toolbox version 5.11.
% %    21-Apr-2014 14:41:04
%
% t2 = cos(phi);
% t3 = sin(phi);
% t4 = phi-phiH;
% footPts = [x+lxC.*t2-lyC.*t3+yF.*sin(t4);y+lxC.*t3+lyC.*t2-yF.*cos(t4)];
% end

function f_cg = F_CoriGravSLIP(x,y,phi,yF,phiH,dx,dy,dphi,dyF,dphiH,g,lxC,lyC,mS,mF,jS,jF,kF,kF0,bF)
%F_CORIGRAV
%    F_CG = F_CORIGRAV(X,Y,PHI,YF,PHIH,DX,DY,DPHI,DYF,DPHIH,G,LXC,LYC,mS,MF,JS,JF,KF,KF0,BF)

%    This function was generated by the Symbolic Math Toolbox version 5.11.
%    11-May-2014 14:15:08
t2 = phi-phiH;
t3 = sin(t2);
t4 = cos(t2);
t5 = dphi.^2;
t6 = dphiH.^2;
t7 = kF0+yF;
t8 = dphi.*dyF.*2.0;
t9 = g.*t3;
t10 = t8+t9-dphiH.*dyF.*2.0;
f_cg = [-mF.*(dphi-dphiH).*(dyF.*t4.*2.0-dphi.*kF0.*t3+dphiH.*kF0.*t3-dphi.*t3.*yF+dphiH.*t3.*yF);-g.*mF-g.*mS-dphi.*dyF.*mF.*t3.*2.0+dphiH.*dyF.*mF.*t3.*2.0-kF0.*mF.*t4.*t5-kF0.*mF.*t4.*t6-mF.*t4.*t5.*yF-mF.*t4.*t6.*yF+dphi.*dphiH.*kF0.*mF.*t4.*2.0+dphi.*dphiH.*mF.*t4.*yF.*2.0;-mF.*t7.*t10;-kF.*yF+g.*mF.*t4+kF0.*mF.*t5+kF0.*mF.*t6+mF.*t5.*yF+mF.*t6.*yF-dphi.*dphiH.*kF0.*mF.*2.0-dphi.*dphiH.*mF.*yF.*2.0;mF.*t7.*t10];
end

% function cont_pointSLIP = ContactPoint(x,y,phi,yF,phiH,g,lxC,lyC,mS,mF,jS,jF,kF,kF0,bF)
% %CONTACTPOINT
% %    CONT_POINT = CONTACTPOINT(X,Y,PHI,YF,PHIH,G,LXC,LYC,mS,MF,JS,JF,KF,KF0,BF)
%
% %    This function was generated by the Symbolic Math Toolbox version 5.11.
% %    21-Apr-2014 14:41:02
%
% t2 = cos(phi);
% t3 = sin(phi);
% t4 = phi-phiH;
% cont_point = [x+lxC.*t2-lyC.*t3+yF.*sin(t4);y+lxC.*t3+lyC.*t2-yF.*cos(t4)];
% end

function J = ContactJacobianSLIP(x,y,phi,yF,phiH,g,lxC,lyC,mS,mF,jS,jF,kF,kF0,bF)
%CONTACTJACOBIAN
%    J = CONTACTJACOBIAN(X,Y,PHI,YF,PHIH,G,LXC,LYC,mS,MF,JS,JF,KF,KF0,BF)

%    This function was generated by the Symbolic Math Toolbox version 5.11.
%    21-Apr-2014 14:41:02

t2 = phi-phiH;
t3 = cos(t2);
t4 = kF0+yF;
t5 = t3.*t4;
t6 = sin(t2);
t7 = t4.*t6;
J = reshape([1.0,0.0,0.0,1.0,t5,t7,t6,-t3,-t5,-t7],[2, 5]);
end

function dJdtTIMESdqdt = ContactJacobianDtTIMESdqdtSLIP(x,y,phi,yF,phiH,dx,dy,dphi,dyF,dphiH,g,lxC,lyC,mS,mF,jS,jF,kF,kF0,bF)
%CONTACTJACOBIANDTTIMESDQDT
%    DJDTTIMESDQDT = CONTACTJACOBIANDTTIMESDQDT(X,Y,PHI,YF,PHIH,DX,DY,DPHI,DYF,DPHIH,G,LXC,LYC,mS,MF,JS,JF,KF,KF0,BF)

%    This function was generated by the Symbolic Math Toolbox version 5.11.
%    21-Apr-2014 14:41:03
t2 = phi-phiH;
t3 = sin(t2);
t4 = dphi-dphiH;
t5 = cos(t2);
dJdtTIMESdqdt = [t4.*(dyF.*t5.*2.0-dphi.*kF0.*t3+dphiH.*kF0.*t3-dphi.*t3.*yF+dphiH.*t3.*yF);t4.*(dyF.*t3.*2.0+dphi.*kF0.*t5-dphiH.*kF0.*t5+dphi.*t5.*yF-dphiH.*t5.*yF)];
end