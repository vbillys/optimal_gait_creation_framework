% *************************************************************************
% 
% The produced functions describe the dynamics ('M', 'f_cg') and the
% kinematics of the contact point ('cont_point', 'J'),
% CoGs ('CoGs'), and links ('links', 'footPts') of a passive dynamic biped. 
% 
%
% Input:  - NONE
% Output: - function M             = MassMatrix(q, param)
%         - function f_cg          = F_CoriGrav(q, dqdt, param)
%         - function cont_point    = ContactPoint(q, param)
%         - function J             = ContactJacobian(q, param)
%         - function CoGs          = CoGPositions(q, param)
%         - function links         = LinkPositions(q, param)
%         - function footPts       = FootPtPositions(q, param)
%
% where q are the generalized coordinates q = [x y gamma alpha], dqdt the
% generalized speeds dqdt = [dx dy dgamma dalpha], and param is a reduced
% vector of mass and kinematic parameters param = [l_0 l2x l2y rFoot gx gy
% m1 m2 j2].    
%
% NOTE:  The constructed functions are not called directly, but through a
% number of wrapper functions which reflect the definition of the
% state vector 'y' and parameter vector 'p'.
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a 
% (uses the symbolic math toolbox)
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also FLOWMAP, JUMPMAP, JUMPSET, GRAPHIC2DSIMPLELINKCLASS,
%            MASSMATRIXWRAPPER, GRAPHICALKINEMATICSWRAPPER,
%            CONTACTKINEMATICSWRAPPER, F_CORIGRAVWRAPPER.


%% Definitions
% Minimal coordinates...
syms gamma alpha
q    = [gamma alpha];
% ... and minimal speeds
syms dgamma dalpha
dqdt = [dgamma dalpha];


% Define the necessary parameter subset:
% Gravity
syms gx gy
% Segment dimensions:
syms l_0 l2x l2y rFoot
% Masses/Inertia;
syms m1 m2
syms j2
param = [l_0 l2x l2y rFoot gx gy m1 m2 j2];

%% DYNAMICS (obtained via the Euler-Lagrange equation)

% CoG-orientations (from kinematics):
CoG_MB_ang     = sym(0); % Main body is always vertical
CoG_Stance_ang = gamma;
CoG_Swing_ang  = gamma + alpha;
% CoG-positions (from kinematics):
% (the reference position is ground level at the position of contact for
% gamma = 0) 
StanceFootCenter = [-gamma*rFoot;
                           rFoot];

CoG_MB     = StanceFootCenter - [+(l_0-rFoot) * sin(CoG_Stance_ang);
                                 -(l_0-rFoot) * cos(CoG_Stance_ang)];
CoG_Stance = CoG_MB + [+l2x * sin(CoG_Stance_ang) + l2y * cos(CoG_Stance_ang) ;
                       -l2x * cos(CoG_Stance_ang) + l2y * sin(CoG_Stance_ang)];
CoG_Swing  = CoG_MB + [+l2x * sin(CoG_Swing_ang) + l2y * cos(CoG_Swing_ang) ;
                       -l2x * cos(CoG_Swing_ang) + l2y * sin(CoG_Swing_ang)];                   
         
% CoG-velocities (computed via jacobians):
d_CoG_MB     = jacobian(CoG_MB,q)*dqdt.';
d_CoG_Stance = jacobian(CoG_Stance,q)*dqdt.';
d_CoG_Swing  = jacobian(CoG_Swing,q)*dqdt.';
d_CoG_Stance_ang = jacobian(CoG_Stance_ang,q)*dqdt.';
d_CoG_Swing_ang  = jacobian(CoG_Swing_ang,q)*dqdt.';

% Potential Energy (due to gravity):
V = -CoG_MB(1)*m1*gx - CoG_Stance(1)*m2*gx - CoG_Swing(1)*m2*gx + ...
    -CoG_MB(2)*m1*gy - CoG_Stance(2)*m2*gy - CoG_Swing(2)*m2*gy;

V = simple(factor(V));

% Kinetic Energy:         
T = 0.5 * (m1 * sum(d_CoG_MB.^2) + ...
           m2 * sum(d_CoG_Stance.^2) + ...
           m2 * sum(d_CoG_Swing.^2) + ...
           j2 * d_CoG_Stance_ang^2 + ...
           j2 * d_CoG_Swing_ang^2);
T = simple(factor(T));

% Lagrangian:
L = T-V;
% Partial derivatives:
dLdq   = jacobian(L,q).';
dLdqdt = jacobian(L,dqdt).';
      
% Compute Mass Matrix:
M = jacobian(dLdqdt,dqdt);
M = simple(M);

% Compute the coriolis and gravitational forces:
dL_dqdt_dt = jacobian(dLdqdt,q)*dqdt.';
f_cg = dLdq - dL_dqdt_dt;
f_cg = simple(f_cg);

% The equations of motion are given with these functions as:   
% M * dqddt = f_cg(q, dqdt) + u;

%% KINEMATICS (for graphical output)
% Joint-positions:
Hip  = CoG_MB;
SwingFootCenter = Hip + [+(l_0-rFoot) * sin(CoG_Swing_ang);
                         -(l_0-rFoot) * cos(CoG_Swing_ang)];
StanceFootCenter = Hip + [+(l_0-rFoot) * sin(CoG_Stance_ang);
                          -(l_0-rFoot) * cos(CoG_Stance_ang)];
                     
                     
% CoGs (incl orientation of the segments):
CoGs = [CoG_MB,     CoG_Stance,     CoG_Swing;
        CoG_MB_ang, CoG_Stance_ang, CoG_Swing_ang];

% Links (or rather: joint positions)
links = [StanceFootCenter, Hip, SwingFootCenter];

% Position of the foot points:     
footPts = [StanceFootCenter, SwingFootCenter];

%% CONTACT DYNAMICS 
% Height of the swing leg of the contact point:
contPoint = simple(SwingFootCenter-[0;rFoot]);
contHeight = contPoint(2);

%% The contact dynamics are processed in a 4DoF floating-base system. This
%% makes it easier to include the post-impact constraint (the velocity of
%% the impacting foot point comes to a rest) in the computation. 
% Define additional generalized coordinates:
syms x y
syms dx dy
% Define extended coordinate vector for contact collision:
q_cont = [x y gamma alpha];
dqdt_cont = [dx dy dgamma dalpha];

% Re-compute the CoG positions (in a floating-base system). The angular
% definitions remain identical:
CoG_MB     = [x;
              y];
CoG_Stance = CoG_MB + [+l2x * sin(CoG_Stance_ang) + l2y * cos(CoG_Stance_ang) ;
                       -l2x * cos(CoG_Stance_ang) + l2y * sin(CoG_Stance_ang)];
CoG_Swing  = CoG_MB + [+l2x * sin(CoG_Swing_ang) + l2y * cos(CoG_Swing_ang) ;
                       -l2x * cos(CoG_Swing_ang) + l2y * sin(CoG_Swing_ang)];                   
% CoG-velocities (computed via jacobians):
d_CoG_MB     = jacobian(CoG_MB,q_cont)*dqdt_cont.';
d_CoG_Stance = jacobian(CoG_Stance,q_cont)*dqdt_cont.';
d_CoG_Swing  = jacobian(CoG_Swing,q_cont)*dqdt_cont.';
d_CoG_MB_ang     = jacobian(CoG_MB_ang,q_cont)*dqdt_cont.';
d_CoG_Stance_ang = jacobian(CoG_Stance_ang,q_cont)*dqdt_cont.';
d_CoG_Swing_ang  = jacobian(CoG_Swing_ang,q_cont)*dqdt_cont.';
% Potential Energy is not needed (We only need the mass matrix)
% Kinetic Energy:         
T = 0.5 * (m1 * sum(d_CoG_MB.^2) + ...
           m2 * sum(d_CoG_Stance.^2) + ...
           m2 * sum(d_CoG_Swing.^2) + ...
           j2 * d_CoG_Stance_ang^2 + ...
           j2 * d_CoG_Swing_ang^2);
T = simple(factor(T));
% Lagrangian (ignoring potential energy):
L = T;
% Compute Mass Matrix:
dLdqdt = jacobian(L,dqdt_cont).';
M_cont = jacobian(dLdqdt,dqdt_cont);
M_cont = simple(M_cont);

% Contact point and contact Jacobian:
StanceFootCenter = CoG_MB + [+(l_0-rFoot) * sin(CoG_Stance_ang);
                             -(l_0-rFoot) * cos(CoG_Stance_ang)];
contPoint = StanceFootCenter - [0;
                                rFoot];
% Contact Jacobian:
% NOTE: The round feet introduce a non-holonomic contact constraint.  I.e.
% the Jacobian needs to be expanded afterwards with the foot rotation:
J_cont = jacobian(contPoint,q_cont) + [0,0,rFoot,0;
                                       0,0,0,    0];
J_cont = simple(J_cont);                            



%% Create MATLAB-functions:
if ~exist('AutoGeneratedFcts','dir')
    mkdir('AutoGeneratedFcts')
end

matlabFunction(M,'file','AutoGeneratedFcts\MassMatrix','vars',[q, param]);
matlabFunction(f_cg,'file','AutoGeneratedFcts\F_CoriGrav','vars',[q, dqdt, param]);

matlabFunction(contHeight,'file','AutoGeneratedFcts\ContactHeight','vars',[q, param]);
matlabFunction(J_cont,'file','AutoGeneratedFcts\ContactJacobian','vars',[q_cont, param]);
matlabFunction(M_cont,'file','AutoGeneratedFcts\ContactMassMatrix','vars',[q_cont,param]);

matlabFunction(CoGs,'file','AutoGeneratedFcts\CoGPositions','vars',[q, param]);
matlabFunction(links,'file','AutoGeneratedFcts\LinkPositions','vars',[q, param]);
matlabFunction(footPts,'file','AutoGeneratedFcts\FootPtPositions','vars',[q, param]);